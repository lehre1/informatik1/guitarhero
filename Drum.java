public class Drum {
    private FC_RingBuffer auslenkung;           // Auslenkung der Saite
    private static double DECAY = 0.999999; // Daempfungsfaktor
    private int time = 0;
    public Drum(double frequency) {
	final int N = (int) Math.round(StdAudio.SAMPLE_RATE / frequency);
	auslenkung = new FC_RingBuffer(N);
	for (int i = 0; i < N; i++)
	    auslenkung.enqueue(0.0);
    }
    public Drum(double[] init) {
	final int N = init.length;
	auslenkung = new FC_RingBuffer(N);
	for (int i = 0; i < N; i++)
	    auslenkung.enqueue(init[i]);
    }
    public void pluck() {
	final int N = auslenkung.size();
	for (int i = 0; i < N; i++) {
            auslenkung.dequeue();
	    auslenkung.enqueue(Math.random() - 0.5);
	}
    }
    public void tic() {
	double b = Math.random();
	auslenkung.dequeue();
	int p = 20;
	double 
	    frst = auslenkung.peek(p),
	    scnd = auslenkung.peek(p+1),
	    last = DECAY * (frst + scnd) / 2 * (b < 0.5 ? 1 : -1 );
	auslenkung.enqueue(last);
	time++;
    }
    public double sample() {
	return auslenkung.peek();
    }
    public int time() {
	return time;
    }
    public double[] dump() {
	return auslenkung.dump();
    }
    public void decay(double d) {
        this.DECAY = d;
    }    
    public static void main(String[] args) {
	double CONCERT_A = 440.0;
	double CONCERT_C = CONCERT_A * Math.pow(1.05956, 3.0);
	Drum stringA = new Drum(CONCERT_A);
	Drum stringC = new Drum(CONCERT_C);
    
	while (true) {
	    // check if user has typed a key; if so, process it
	    if (StdDraw.hasNextKeyTyped()) {
		char key = StdDraw.nextKeyTyped();
		if      (key == 'a') { stringA.pluck(); }
		else if (key == 'c') { stringC.pluck(); }
	    }
	    
	    // compute superposition of samples
	    double sample = stringA.sample() + stringC.sample();
	    StdAudio.play(sample);
	    
	    // advance the simulation of each guitar string by one step
	    stringA.tic();
	    stringC.tic();
	}	    
    }
}
