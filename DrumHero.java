public class DrumHero {
    public static void main(String[] args) {
	final String KEYBOARD = 
	    "q2we4r5tz7u8i9op0+\\" +
	    "yxcdfvgbnjmk,.l-# ";
	final char[] keys = new char[KEYBOARD.length()];
	for (int i = 0; i < keys.length; i++)
	    keys[i] = KEYBOARD.charAt(i);

	Drum[] drum = new Drum[37];
	double CONCERT_A = 440.0;


	for (int i = 0; i < keys.length; i++)
	    drum[i] = new Drum(CONCERT_A * Math.pow(2,(i-24)/12.));
	if ( args.length > 0) {
	    int k; 
	    double d;
	    while (!StdIn.isEmpty()) {
		k = StdIn.readInt()+24;
		d = StdIn.readDouble();
		drum[k].pluck();
		// play for d seconds
		for (int j=0; j < StdAudio.SAMPLE_RATE*d;j++) {
		    // compute superposition of samples
		    double sample = 0;
		    for (int i = 0; i < keys.length; i++) {
			sample += drum[i].sample();
			drum[i].tic();
		    }
		    StdAudio.play(sample);
		}	    
	    }
	    return;
	}

	while (true) {
	    // check if user has typed a key; if so, process it
	    if (StdDraw.hasNextKeyTyped()) {
		char key = StdDraw.nextKeyTyped();
		for (int i = 0; i < keys.length; i++)
		    if (key == keys[i]) {
			drum[i].pluck();
		    }
	    }
	    
	    // compute superposition of samples
	    double sample = 0;
	    for (int i = 0; i < keys.length; i++) {
		sample += drum[i].sample();
		drum[i].tic();
	    }
	    StdAudio.play(sample);
	}	    
    }
}
