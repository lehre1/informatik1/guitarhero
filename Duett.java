/* Compilieren mit 
   javac Duett -encoding UTF-8 GuitarHero
*/

public class Duett {
    public static void main(String[] args) {
	if (args.length == 0) {
	    System.out.println("Aufruf: Duett <Gitarren-Anteil>");
	    return;
	}
	final String KEYBOARD = 
	    "q2we4r5tz7u8i9opßü+" +
	    "yxcdfvgbnjmk,.l-# ";
	final char[] keys = new char[KEYBOARD.length()];
	for (int i = 0; i < keys.length; i++)
	    keys[i] = KEYBOARD.charAt(i);

	GuitarString[] guitar = new GuitarString[37];
	Drum[] percussion = new Drum[37];
	double CONCERT_A = 440.0;

	for (int i = 0; i < keys.length; i++) {
	    guitar[i] = new GuitarString(CONCERT_A * Math.pow(2,(i-24)/12.));
	    percussion[i] = new Drum(CONCERT_A * Math.pow(2,(i-36)/12.));
	}
	double m = Double.parseDouble(args[0]);
	if ( m < 0.0 || m > 1.0) m = 1;
	if ( args.length > 1) {
	    int k; 
	    double d;
	    while (!StdIn.isEmpty()) {
		k = StdIn.readInt()+24;
		d = StdIn.readDouble();
		guitar[k].pluck();
		percussion[k].pluck();
		// play for d seconds
		for (int j=0; j < StdAudio.SAMPLE_RATE*d;j++) {
		    // compute superposition of samples
		    double sample = 0;
		    for (int i = 0; i < keys.length; i++) {
			sample += (m*guitar[i].sample()+(1-m)*percussion[i].sample());
			guitar[i].tic();percussion[i].tic();
		    }
		    StdAudio.play(sample);
		}	    
	    }
	    return;
	}

	while (true) {
	    // check if user has typed a key; if so, process it
	    if (StdDraw.hasNextKeyTyped()) {
		char key = StdDraw.nextKeyTyped();
		for (int i = 0; i < keys.length; i++)
		    if (key == keys[i]) {
			guitar[i].pluck();
			percussion[i].pluck();
		    }
	    }
	    
	    // compute superposition of samples
	    double sample = 0;
	    for (int i = 0; i < keys.length; i++) {
		sample += (m*guitar[i].sample()+(1-m)*percussion[i].sample());
		guitar[i].tic();percussion[i].tic();
	    }
	    StdAudio.play(sample);
	}



	    
    }
}
