public class FC_RingBuffer          // Fixed Capacity RingBuffer 
{
    private double[] a;             // 
    private int n = 0;              // Anzahl gespeicherter Werte
    private int first = 0;

    public FC_RingBuffer(int capacity) {
	a = new double[capacity];
    }
    public int size() {             // Wieviele Werte sind gespeichert?
	return n;
    }
    public boolean isEmpty() 
    {   return (n == 0);}
    public boolean isFull() { 
	return ( n == a.length); 
    }
    public void enqueue(double x) { // ...
	if ( isFull())
	    throw new RuntimeException("\n\nPufferueberlauf!\n");
	int free = (first + n) % a.length;
	a[free++] = x; 
	n++;
    }
    public double dequeue() {
	if (isEmpty())
	    throw new RuntimeException("\n\nPufferunterlauf!\n");
	double x = a[first++];
	first %= a.length;
	n--;
	return x;
    }
    public double peek() {
	return this.a[first];
    }
    public double peek(int idx) {
	return this.a[(first+idx) % a.length];
    }
    public double[] dump() {
	final int N = this.size();
	double[] d = new double[N];
	for (int i = 0; i<N; i++)
	    d[i] = a[i];
	return d;
    }
    public static void main( String[] args) {
	int capacity;
	if (args.length == 0) capacity = 10;
	else capacity = Integer.parseInt(args[0]);
	System.out.println("Ringpuffer mit Kapazitaet " + 
			   capacity + " wird angelegt.");
	FC_RingBuffer b = new FC_RingBuffer(capacity);
	b.enqueue(StdIn.readDouble());     // lesen und einspeichern
	b.enqueue(StdIn.readDouble());     // lesen und einspeichern
	b.enqueue(StdIn.readDouble());     // lesen und einspeichern
	System.out.println(b.dequeue());// ausspeichern und berichten
	System.out.println(b.dequeue());// ausspeichern und berichten
	b.enqueue(StdIn.readDouble());     // lesen und einspeichern
	b.enqueue(StdIn.readDouble());     // lesen und einspeichern
	b.enqueue(StdIn.readDouble());     // lesen und einspeichern
	System.out.println(b.dequeue());// ausspeichern und berichten
	b.enqueue(StdIn.readDouble());     // lesen und einspeichern
	System.out.println(b.dequeue());// ausspeichern und berichten
	System.out.println(b.dequeue());// ausspeichern und berichten
	System.out.println(b.dequeue());// ausspeichern und berichten
	System.out.println(b.dequeue());// ausspeichern und berichten
    }
}

