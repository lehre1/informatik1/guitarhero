/* Compilieren mit 
   javac -encoding UTF-8 *java 
*/

public class GuitarHero {
    public static void main(String[] args) {
	final String KEYBOARD = 
	    "q2we4r5tz7u8i9opßü+" +
	    "yxcdfvgbnjmk,.l-# ";
	final char[] keys = new char[KEYBOARD.length()];
	for (int i = 0; i < keys.length; i++)
	    keys[i] = KEYBOARD.charAt(i);

	GuitarString[] guitar = new GuitarString[37];
	double CONCERT_A = 440.0;

	for (int i = 0; i < keys.length; i++)
	    guitar[i] = new GuitarString(CONCERT_A * Math.pow(2,(i-24)/12.));
	if ( args.length > 0) {
            if (args.length >= 2) {
                double dec = Double.parseDouble(args[1]);
                for (int i = 0; i < keys.length; i++)
                    guitar[i].decay(dec);
            }
	    double[] w = {0., 1., 0.};
	    if (args.length == 5) {
		w[0] = Double.parseDouble(args[2]);
		w[1] = Double.parseDouble(args[3]);
		w[2] = Double.parseDouble(args[4]);
	    } 

	    int k; 
	    double d;
	    while (!StdIn.isEmpty()) {
		k = StdIn.readInt()+24;
		d = StdIn.readDouble();
		guitar[k].pluck(w);
		// play for d seconds
		for (int j=0; j < StdAudio.SAMPLE_RATE*d;j++) {
		    // compute superposition of samples
		    double sample = 0;
		    for (int i = 0; i < keys.length; i++) {
			sample += guitar[i].sample();
			guitar[i].tic();
		    }
		    StdAudio.play(sample);
		}	    
	    }
	    return;
	}

	while (true) {
	    // check if user has typed a key; if so, process it
	    if (StdDraw.hasNextKeyTyped()) {
		char key = StdDraw.nextKeyTyped();
		for (int i = 0; i < keys.length; i++)
		    if (key == keys[i]) {
			guitar[i].pluck();
		    }
	    }
	    
	    // compute superposition of samples
	    double sample = 0;
	    for (int i = 0; i < keys.length; i++) {
		sample += guitar[i].sample();
		guitar[i].tic();
	    }
	    StdAudio.play(sample);
	}



	    
    }
}
