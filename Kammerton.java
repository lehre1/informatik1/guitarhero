/* Nach Sedgewick/Wayne */ 
public class Kammerton {
    public static void main(String[] args) {
	int sps = 44100;                // Samples pro Sekunde
	int hz = 440;                   // Kammerton A

	System.out.println("Aufruf: java Kammerton [<Dauer in Sekunden> [<Lautstaerke>]]");

	double dauer = 10;           // zehn Sekunden
	if (args.length > 0) 
	    dauer = Double.parseDouble(args[0]);
	int N = (int) (sps * dauer); // Gesamtzahl der Samples
	double lautstaerke = 1.0;
	if (args.length > 1)
	    lautstaerke = Math.min(Math.abs(Double.parseDouble(args[1])),2);
	double[] a = new double[N+1];
	for (int i = 0; i <= N; i++) 
	    a[i] = lautstaerke*Math.sin(2 * Math.PI * hz * i / sps);

	if (args.length > 0) { // graphische Darstellung der .. 
	    // .. Abtastwerte waehrend der ersten 0.025s
	    int n = (int) (sps * 0.025); // entsprichet 1/40 einer Sekunde ...
	    double[] b = new double[n];
	    for (int i = 0; i < n; i++)
		b[i] = a[i];
	    StdDraw.setXscale(0,n);
	    StdDraw.setYscale(-1.25,1);
	    StdDraw.setPenColor(StdDraw.RED);
	    StdDraw.text(n/2,-1.15,"reiner Kammerton, Lautstaerke: " + lautstaerke);
	    StdDraw.setPenColor(StdDraw.BLACK);
	    StdDraw.setPenRadius(0.005);
	    StdStats.plotPoints(b);      // ... 440 / 40 = 11 Sinusschwingungen
	}
	for (int i = 0; i < N; i++)
	    StdAudio.play(a[i]);
    }
}
