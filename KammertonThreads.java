/* TODO .... 
   der Plan ist, gleichzeitig Töne abzuspielen und die Wellenform dazu anzuzeigen 
*/

class PlayConcertA extends Thread {
    public void run() {
        try {
            sleep(50);
        }
        catch(InterruptedException e) {
        }
        StdAudio.play(KammertonThreads.a);
    }
}
class DisplayConcertA extends Thread {
    public void run() {
        StdStats.plotPoints(KammertonThreads.a);
    }
}

public class KammertonThreads {
    public static double[] a;
    public static void main(String[] args) {
        int sps = 44100;
        int hz = 440;
        double dauer = 0.1;
        int N = (int) (sps * dauer); 
        double lautstaerke = 1.0;
        a = new double[N+1];
        for (int i = 0; i <= N; i++) 
            a[i] = lautstaerke*Math.sin(2 * Math.PI * hz * i / sps);
        PlayConcertA    tone = new PlayConcertA();
        DisplayConcertA wave = new DisplayConcertA();
        tone.start();
        wave.start();
        // int n = (int) (sps * 0.025);
        // double[] b = new double[n];
        // for (int i = 0; i < n; i++)
        //     b[i] = a[i];
        // StdDraw.setXscale(0,n);
        // StdDraw.setYscale(-1.25,1);
        // StdDraw.setPenColor(StdDraw.RED);
        // StdDraw.text(n/2,-1.15,"reiner Kammerton, Dauer: " + dauer + ", relative Lautstaerke: " + lautstaerke);
        // StdDraw.setPenColor(StdDraw.BLACK);
        // StdDraw.setPenRadius(0.005);
        // StdStats.plotPoints(b);
    }
}
