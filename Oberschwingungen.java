public class Oberschwingungen {
    // take weighted sum of two arrays
    public static double[] sum(double[] a, double[] b, double awt, double bwt) {
        // precondition: arrays have the same length
        assert (a.length == b.length);
        // compute the weighted sum
        double[] c = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            c[i] = a[i]*awt + b[i]*bwt;    } // gewichtete Aufsummierung
        return c;
    } 
    public static double[] tone(double hz, double duration) { 
        int N = (int) (StdAudio.SAMPLE_RATE * duration);
        double[] a = new double[N+1];
        for (int i = 0; i <= N; i++) { // reine Sinusschwingung:
            a[i] = Math.sin(2 * Math.PI * hz * i / StdAudio.SAMPLE_RATE); } 
        return a; 
    } 
    public static double[] note(int pitch, double t) {
        double hz = 440.0 * Math.pow(2, pitch / 12.0);
        double[] a  = tone(hz, t);
        double[] hi = tone(2*hz, t), lo = tone(hz/2, t); // Obertoene
        double[] h  = sum(hi, lo, .5, .5);
        return sum(a, h, .5, .5);
    }
    public static void main(String[] args) {
	int sps = 44100;                // Samples pro Sekunde
	int hz = 440;                   // Kammerton A

	System.out.println("Aufruf: java Oberschwingungen [<Dauer in Sekunden>]");

	double dauer = 10;           // zehn Sekunden
	if (args.length > 0) 
	    dauer = Double.parseDouble(args[0]);
	int N = (int) (sps * dauer); // Gesamtzahl der Samples
	double[] a = note(0,dauer);

	if (args.length > 0) { // graphische Darstellung der .. 
	    // .. Abtastwerte waehrend der ersten 0.025s
	    int n = (int) (sps * 0.025);
	    double[] b = new double[n];
	    for (int i = 0; i < n; i++)
		b[i] = a[i];
	    StdDraw.setXscale(0,n);
	    StdDraw.setYscale(-1.25,1);
	    StdDraw.setPenColor(StdDraw.RED);
	    StdDraw.text(n/2,-1.15,"Kammerton mit Oberschwingungen, Dauer: " + dauer);
	    StdDraw.setPenColor(StdDraw.BLACK);
	    StdDraw.setPenRadius(0.005);
	    StdStats.plotPoints(b);
	}
	
	for (int i = 0; i <= N; i++) 
	    StdAudio.play(a[i]);
    }
}
