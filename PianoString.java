public class PianoString {
    private FC_RingBuffer auslenkung;    // Auslenkung der Saite
    private double DECAY = 0.99; // Daempfung
    private int time = 0;
    public PianoString(double frequency) {
	// Erinnerung: StdAudio.SAMPLE_RATE == 44100
	final int N = (int) Math.round(StdAudio.SAMPLE_RATE / frequency);
	auslenkung = new FC_RingBuffer(N);
	for (int i = 0; i < N; i++)
	    auslenkung.enqueue(0.0);
    }
    public PianoString(double[] init) {
	final int N = init.length;
	auslenkung = new FC_RingBuffer(N);
	for (int i = 0; i < N; i++)
	    auslenkung.enqueue(init[i]);
    }
    public void pluck() {
	final int N = auslenkung.size();
        double v;
	for (int i = 0; i < N; i++) {
            auslenkung.dequeue();
	    v = Math.sin(2*Math.PI*440*i/StdAudio.SAMPLE_RATE);
	    auslenkung.enqueue(v);
	}
    }
    public void pluck(double[] w) {
	final int N = auslenkung.size();
        double v;
	for (int i = 0; i < N; i++) {
            auslenkung.dequeue();
	    v  = w[0]* Math.sin(2*Math.PI*220*i/StdAudio.SAMPLE_RATE);
            v += w[1] * Math.sin(2*Math.PI*440*i/StdAudio.SAMPLE_RATE);
	    v += w[2] * Math.sin(2*Math.PI*880*i/StdAudio.SAMPLE_RATE);
	    auslenkung.enqueue(v);
	}
    }
    public void tic() {
	double 
	    frst = auslenkung.dequeue(),
	    scnd = auslenkung.peek(),
	    last = DECAY * (frst + scnd) / 2;
	auslenkung.enqueue(last);
	time++;
    }
    public double sample() {
	return auslenkung.peek();
    }
    public int time() {
	return time;
    }
    public double[] dump() {
	return auslenkung.dump();
    }
    public void decay(double d) {
        this.DECAY = d;
    }
    public static void main(String[] args) {
	double CONCERT_A = 440.0;
	double CONCERT_C = CONCERT_A * Math.pow(2, 3.0/12);
        PianoString stringA = new PianoString(CONCERT_A);
        PianoString stringC = new PianoString(CONCERT_C);

        if (args.length > 0) {
            double d = Double.parseDouble(args[0]);
            stringA.decay(d); stringC.decay(d);
        }
	double[] w = {0.25, 0.5, 0.25};
	while (true) {
	    if (StdDraw.hasNextKeyTyped()) {               // Taste gedrueckt?
		char key = StdDraw.nextKeyTyped();         
		if      (key == 'a') { stringA.pluck(); }  // nur 'a' oder ..
		else if (key == 'c') { stringC.pluck(); }  // .. 'c' verarbeiten
		else if (key == 'A') { stringA.pluck(w);}  // nur 'A' oder ..
		else if (key == 'C') { stringC.pluck(w);}  // .. 'C' verarbeiten
	    }
	    
	    // Ueberlagerung der Auslenkungen berechnen
	    double sample = stringA.sample() + stringC.sample();
	    StdAudio.play(sample); // fuer (1/44100)s abspielen
	    // Simulation der beiden Saiten fuer einen Schritt
	    stringA.tic();
	    stringC.tic();
	}	    
    }
}
