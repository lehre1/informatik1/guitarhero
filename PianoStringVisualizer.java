public class PianoStringVisualizer {
    public static void main(String[] args) {
	int steps = 1; // alle wieviele Schritte wird Simulationszustand gezeigt
	int ticks = 0; // tatsaechliche Schrittzahl
	if (args.length > 0) 
	    steps = Integer.parseInt(args[0]);
        
	double CONCERT_A = 440.0;
	PianoString stringA = new PianoString(CONCERT_A);
	if (args.length == 2) {
	    double d = Double.parseDouble(args[1]);
            stringA.decay(d);
        }

	double[] d = stringA.dump();
	final int N = d.length;
	int delay = 20;
	StdDraw.setPenRadius(0.005);
	StdDraw.setXscale(0,N);
	StdDraw.setYscale(-1,1);
	stringA.pluck();
	while (true) {
	    d = stringA.dump();
	    StdDraw.clear();
	    StdDraw.setPenColor(StdDraw.BLACK);
	    StdDraw.line(0,0,N,0);  // x-Achse
	    StdDraw.line(0,-1,0,1); // y-Intervall
	    StdDraw.setPenColor(StdDraw.RED);
	    StdDraw.text(N/2,-0.9,1000*ticks/StdAudio.SAMPLE_RATE+"ms");
	    StdDraw.setPenColor(StdDraw.BLUE);
	    StdStats.plotLines(d);
	    StdDraw.show(delay);
    	    // 'steps' Simulationsschritte durchfuehren
	    for (int i = 0; i<steps; i++) {
		stringA.tic();
		ticks++;
	    }
	}	    
    }
}
