# `GuitarHero` &#x2014; Ein Programmierprojekt für die langen Winterabende &#x2026;

Nach: Sedgewick/Wayne

Ziel ist es, den Klang von Saiteninstrumenten zu simulieren:
<http://introcs.cs.princeton.edu/java/assignments/guitar.html>

(siehe <https://de.wikipedia.org/wiki/Guitar_Hero> zu anderer
Bedeutung von "Guitar Hero")

# Prinzipien der Audio-Wiedergabe (Wiederholung, siehe auch Kapitel 3 im Skript)

-   Schall = Schwankungen des Luftdrucks um mittleren Wert
-   Klang = Wahrnehmung von Schall über Trommelfell, das durch
    periodische Luftdruckschankungen angeregt wird
-   Frequenz entspricht Tonhöhe, Hörbereich 16Hz - 20kHz, Amplitude
    entspricht Lautstärke
-   Beispiel Kammerton: Sinusschwingung mit 440Hz
    -   eine Schwingung dauert 1/440 s = 0.00227s, `java Sampling
              0.00227` zeigt Abtastwerte mit Frequenz 44.1kHz &asymp; 100 \*
        440Hz, also ca. 100 Abtastwerte, `java Sampling` zeigt etwa 11
        Schwingungen (0.025s)
    -   generell werden die Abtastwerte so erzeugt:
        
            int sps = 44100;           // samples per second: Abtastrate
            int N = (int) dauer * sps; // Anzahl der Abtastwerte
            double[] a = new a[N]; 
            for (int i = 0; i < a.length; i++)
               a[i] = lautstaerke * Math.sin(2 * Math.PI * 440 * i/sps);
    -   Kurve ist aus Abtastwerten rekonstruierbar, wenn Abtastfrequenz
        > 2\*größte im Signal enthaltene Frequenz ist
    -   44.1kHz ist an menschlichen Hörbereich angepasst: 16Hz - 20kHz
-   Abtastwerte im Feld `double[] a` können wiedergegeben werden
    durch `StdAudio.play(a)` oder (etwas ueberraschend!)  durch:
    
        for (int i = 0; i < a.length; i++)
             StdAudio.play(a[i]); // Sample fuer 1/44100 s abspielen

# Kammerton

-   `java Kammerton` gibt den Kammerton für 10s wieder
-   `java Kammerton 1` gibt ihn für 1s wieder, stellt vorher eine
    0.025s-Abtastung graphisch dar

# Noten

-   Kammerton wird hier (nicht ganz traditionell) notiert als a1, eine
    Oktave höher ist a2, eine Oktave tiefer a0
-   eine Oktave entspricht Frequenzverdopplung: a2 &#x2014; 880Hz,
    a0 &#x2014; 220Hz
-   die Oktave ist unterteilt in 12 *Halbtonschritte*, bei jedem wird
    die Frequenz erhöht um \(\alpha = 2^{1/12.}\), so dass nach
    Entfernung 12 die Frequenz verdoppelt ist
-   die *Entfernung* eines Tons vom Kammerton ist eine eine ganze
    Zahl \(i\) (`pitch`), seine Frequenz ist dann \(440 \cdot
        \alpha^i\)
-   eine Note wird beschrieben durch `pitch` und `duration` (Dauer)
-   `PlayThatTune` liest von der Standardeingabe Noten als (`pitch`,
    `duration`)-Paare und spielt sie ab
    -   Beispiel: `java PlayThatTune < elise.txt`

# Obertöne

-   die reine Sinusschwingung des Kammertons klingt sehr synthetisch
    (nicht ästhetisch)
-   Musikinstrumente erzeugen keine reinen Töne, denn sie schwingen
    in unterschiedlichen Frequenzen mit, z.B. in den Tönen einen
    Oktave höher und niedriger
-   die Überlagerung dieser Schwingungen erzeugt natürlichere Töne,
    die Hauptschwingung sollte dabei größere Amplitude haben als die
    anderen, die Nebenschwingungen (z.B. Verhältnis (1/2, 1/4, 1/4))
    -   `java Oberschwingungen 1` erzeugt eine Sekunde lang die
        (1/2,1/4,1/4)-Mischung aus a1, a0 und a2 und stellt die
        Überlagerung zusätzlich graphisch dar (ein 0.025s-Ausschnitt)
    -   `java PlayThatTuneDeluxe < elise.txt`
-   man beachte, wie in `Oberschwingungen.java` Felder als Argumente
    bzw. als Rückgabewerte verwendet werden

# Instrumentenklang

-   auch der überlagerte Klang ist nicht sehr natürlich, z.B. wird
    die Dämpfung vernachlässigt: ein nicht dauerhaft gespielter Ton
    wird immer leiser
-   eine gespannte Gitarrensaite gibt beim Anspielen eine
    Anfangsauslenkung an einer Stelle durch mechanische Schwingungen
    weiter, die an den Spannpunkten reflektiert werden, dies wird
    durch die sogenannte [Wellengleichung](https://de.wikipedia.org/wiki/Wellengleichung) beschrieben, die unter
    kontrollierten Anfangsbedingungen gelöst werden kann
-   das Zupfen einer Gitarrensaite ist allerdings nicht so gut
    kontrolliert, entspricht auf kurzer Distanz eher einer
    Zufallsinitialisierung (physikalisch: weißes Rauschen)
-   unter diesen Bedingungen (und zusätzlicher Dämpfung) kann man
    versuchen, die Lösung der Wellengleichung zu simulieren, das
    macht der [Karplus-Strong-Algorithmus](https://de.wikipedia.org/wiki/Karplus-Strong-Algorithmus)

# Vorbereitung: Ringpuffer mit gegebener Kapazität

Wie verhält sich ein Objekt, dass eine (maximale) Anzahl von
`double`-Werten speichern kann und nach fallendem\`\`Alter''
ausliefern bzw. aus dem Speicher entfernen kann?: Im wesentlichen
wie ein Wartezimmer beim Arzt mit der Zusatzforderung, dass neu
hinzukommende Patienten sich immer auf den Platz unmittelbar rechts
neben den unmittelbar davor gekommenen setzen. Das *kann* natürlich
nur funktionieren, wenn (1) klar ist, was \`\`unmittelbar rechts neben''
bedeutet (also Stühle im Kreis anordnen) und (2) nicht mehr Patienten versorgt
werden müssen, als Plätze im Wartezimmer sind. 

Wir nennen die Klasse, die solche Objekte beschreibt `FC_RingBuffer`
(fixed capacity ring buffer) und beschreiben die Anforderungen
formal:

-   Instanzmethoden
    -   `int size()` (Füllstand), `isEmpty()`, `isFull()` entsprechend
    -   `enqueue(double)` - Wert hinzufügen (sofern
    -   `double dequeue()` - den *ältesten* Wert entnehmen (also unter
        allen momentan gehaltenen Werten den zuerst gespeicherten)
    -   `double peek()` - den ältesten Wert liefern, aber noch nicht
        entnehmen
-   Konstruktor `FC_RingBuffer(int)`

Zusätzlich ist für uns noch dieses Verhalten nützlich, um den Inhalt
visualisieren zu können:

-   `double[] dump()` - ein Feld liefern, das den kompletten aktuellen Inhalt
    widerspiegelt (in der Anordnung: ältester, zweitältester, &#x2026;)

-   Implementierung: auf Grundlage der `private`-Attribute `double[] a`, `int first`, `int n`
-   Testen mit `java FC_RingBuffer <Kapazität>`

In unserem Anwendungsfall wird der Puffer praktisch immer voll sein.

# Der Algorithmus

ausgelesene Werte werden modifiziert rückgekoppelt

-   **initialisiere:** -   erzeuge Ringpuffer mit Kapazität 
        entsprechend der Frequenz
    -   Belege den Ringpuffer mit Zufallswerten
-   **iteriere:** -   entnehme einen Wert `x` und spiele ihn als Sample ab
    -   bestimme den Folgewert `y`
    -   *Feedback*: füge den *gedämpften* Mittelwert \(\beta\cdot(x+y)/2\) hinzu
        mit \(0<beta<1\) (je größer der Wert, desto länger der Nachklang)

siehe `GuitarString.java`

-   Demo 1: `java GuitarStringVisualizer` bzw.  `java
        GuitarStringVisualizer <Faktor>` (geeignete Werte für den
    Geschwindigkeitsfaktor sind 10, 30, 100; 1000 entspricht auf
    meinem Laptop etwa Realzeit)
-   Demo 2: `java GuitarString`  (Test-Unit der Klasse)
    -   Besonderheit: es öffnet sich ein Fenster mit der \`\`Leinwand'', ist
        es aktiv, so reagiert die Anwendung auf Tastenanschläge a und c
        (andere werden ignoriert)
-   Demo 3: `java GuitarHero` reagiert auf alle Tasten aus 
    `q2we4r5tz7u8i9opßü+yxcdfvgbnjmk,.l-#` (ca. 3 Oktaven)
-   Demo 4: `java GuitarHero spiele < elise.txt` (das
    Kommandozeilenargument sorgt dafür, dass die Anwendung Daten von
    der Standardeingabe entgegennimmt, anstelle `spiele` kann auch
    irgendetwas anderes stehen)

# Modifikationen

Es sind einige Feedback-Varianten bekannt, die andere
Instrumentenklänge simulieren, z.B. für Trommeln: der Mittelwert
einiger späterer Werte wird bestimmt, zufällig mit +1 oder -1
multipliziert und dem Puffer hinzugefügt.

-   Demos: `java DrumHero`, `java DrumHero spiele < elise.txt`

(siehe: Karplus, Kevin; Strong, Alex (1983). *Digital Synthesis of Plucked String and Drum Timbres*. Computer Music Journal. MIT Press. 7 (2): 43–55. <10.2307/3680062>)

# Duett

-   Demo: z.B. `java Duett 0.2 spiele < elise.txt` [die Gitarre geht
    mit Gewicht 0.2 ein, die Trommel mit 0.8]
-   klingt weniger gut, als vielleicht erhofft &#x2026;