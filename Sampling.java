/* Kammerton (440Hz) mit gegebener Abtastrate (Samples / sec) 
   0.025 Sekunden abtasten, Samples plotten 

   Aufruf: Sampling [<Dauer> [<Abtastrate>]]
*/ 

public class Sampling {
    public static void main(String[] args) {
	double dauer = 0.025;
	int sps = 44100;
	System.out.println(
			   "Dauer:       " + dauer + "s\n"+
			   "Abstastrate: " + sps + "sps\n" +
			   "(einstellbar mit Kdozeilenparametern)");
	if (args.length > 0) 
	    dauer = Double.parseDouble(args[0]);
	if (args.length > 1)    
	    sps = Integer.parseInt(args[1]); 
	    
	int N = (int) (sps * dauer);
	double[] a = new double[N+1];
	for (int i = 0; i <= N; i++) {
	    a[i] = Math.sin(2 * Math.PI * 440 * i / sps);
	}
	StdDraw.setXscale(0,N);
	StdDraw.setYscale(-1.25,1);
	StdDraw.setPenColor(StdDraw.RED);
	StdDraw.text(N/2,-1.15,"Dauer: " + dauer + "s, Abtastfrequenz: " + sps + "Hz");
	StdDraw.setPenColor(StdDraw.BLACK);
	StdDraw.setPenRadius(0.005);
	StdStats.plotPoints(a);
    }
}
